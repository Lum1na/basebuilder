﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GLOBAL 
{
    public static bool DEBUG_BUILDING = false;
    public static float OBJECT_SIZE = 3.25f;
    public static float MAX_OBJECT_OVERLAP = 0.25f;
    public static float STRUCTURE_COLLISIONS_UPDATE_INTERVAL = 0.01f;
}
