﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class BaseStructure : MonoBehaviour
{
    [SerializeField] private EBuildingType buildType;
    [SerializeField] private Material ValidMaterial;
    [SerializeField] private Material InvalidMaterial;
    [Tooltip("The collider used to determine if object overlap still allowes palcement of the structure")]
    [SerializeField] private BoxCollider PlacementRestrictionCollider;

    [Space]
    [SerializeField] private PositionHelper[] adjacentFloorPositions;
    [SerializeField] private PositionHelper[] adjacentWallPositions;
    [SerializeField] private PositionHelper[] adjacentDirectionalFloorPositions;
    [SerializeField] private PositionHelper[] adjacentDirectionalWallPositions;

    //Helper functions for the editor script to make it easier to change size of the object
    public PositionHelper[] AdjacentFloorPositions
    {
        set { adjacentFloorPositions = value; }
        get { return adjacentFloorPositions; }
    }
    public PositionHelper[] AdjacentWallPositions
    {
        set { adjacentWallPositions = value; }
        get { return adjacentWallPositions; }
    }
    public PositionHelper[] AdjacentDirectionalFloorPositions
    {
        set { adjacentDirectionalFloorPositions = value; }
        get { return AdjacentDirectionalFloorPositions; }
    }
    public PositionHelper[] AdjacentDirectionalWallPositions
    {
        set { adjacentDirectionalWallPositions = value; }
        get { return adjacentDirectionalWallPositions; }
    }

    public List<BaseStructure> FloorNeighbours
    {
        get { return adjacentFloorNeighbours; }
    }
    public List<BaseStructure> WallNeighbours
    {
        get { return adjacentWallNeighbours; }
    }
    public List<BaseStructure> DirectionalFloorNeighbours
    {
        get { return adjacentDirectionalFloorNeighbours; }
    }
    public List<BaseStructure> DirectionalWallNeighbours
    {
        get { return adjacentDirectionaWallNeighbours; }
    }

    //List of all the neighbors 
    private List<BaseStructure> adjacentDirectionalFloorNeighbours = new List<BaseStructure>();
    private List<BaseStructure> adjacentDirectionaWallNeighbours = new List<BaseStructure>();
    private List<BaseStructure> adjacentFloorNeighbours = new List<BaseStructure>();
    private List<BaseStructure> adjacentWallNeighbours = new List<BaseStructure>();
    private PositionHelper[] allPositions;

    protected List<Collider> currentCollisions = new List<Collider>();
    protected List<ContactPoint[]> currentContactPoints = new List<ContactPoint[]>();

    private MeshRenderer meshRender;
    public EBuildingType BuildType
    {
        get { return buildType; }
    }

    protected bool canBePlaced = false;
    public virtual bool CanBePlaced
    {
        get { return canBePlaced; }
        set
        {
            if (value && ValidMaterial && meshRender)
                meshRender.material = ValidMaterial;
            else if (value == false && InvalidMaterial && meshRender)
                meshRender.material = InvalidMaterial;
            canBePlaced = value;
        }
    }

    private bool isPlaced;
    public bool IsPlaced
    {
        get { return isPlaced; }
        set
        {
            isPlaced = value;

            if (value)
            {
                //Cancel collision check updates
                CancelInvoke("UpdateCanBePlaced");
                //set to building layer
                gameObject.layer = LayerMask.NameToLayer("Building");
                //add all colliding structures as neighbours
                AddNeighboursForCollisions();
                //clear collision list
                currentCollisions = new List<Collider>();
                currentContactPoints = new List<ContactPoint[]>();

                //invoke event and let every subsriber know that structure has been placed;
                OnStructurePlaced?.Invoke();
            }
        }
    }

    private BaseStructure compareStructure;
    public BaseStructure CompareStructure
    {
        get { return compareStructure; }
        set { compareStructure = value; }
    }

    public delegate void OnStructurePlacedEvent();
    public OnStructurePlacedEvent OnStructurePlaced;

    private void Start()
    {
        Rigidbody rbody = GetComponent<Rigidbody>();
        if (rbody)
        {
            rbody.useGravity = false;
            rbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        meshRender = GetComponentInChildren<MeshRenderer>();
        CreateAllPostitionsArray();
        InvokeRepeating("UpdateCanBePlaced", 0, GLOBAL.STRUCTURE_COLLISIONS_UPDATE_INTERVAL);
    }

    protected void CreateAllPostitionsArray()
    {
        allPositions = new PositionHelper[adjacentFloorPositions.Length +
                                                                 adjacentWallPositions.Length +
                                                                 adjacentDirectionalFloorPositions.Length +
                                                                 adjacentDirectionalWallPositions.Length];


        Array.Copy(adjacentFloorPositions, allPositions, adjacentFloorPositions.Length);
        Array.Copy(adjacentWallPositions, 0, allPositions, adjacentFloorPositions.Length, adjacentWallPositions.Length);
        Array.Copy(adjacentDirectionalFloorPositions, 0, allPositions, adjacentFloorPositions.Length + adjacentWallPositions.Length, adjacentDirectionalFloorPositions.Length);
        Array.Copy(adjacentDirectionalWallPositions, 0, allPositions, adjacentFloorPositions.Length + adjacentWallPositions.Length + adjacentDirectionalFloorPositions.Length, adjacentDirectionalWallPositions.Length);
    }

    protected void UpdateCanBePlaced()
    {
        if (currentCollisions.Count != 0 && IsPlaced == false)
        {
            CanBePlaced = HasValidStructureCollision();
        }
        else if (IsPlaced == false && compareStructure == null && buildType == EBuildingType.Foundation)
        {
            CanBePlaced = true;
        }
        else
        {
            CanBePlaced = false;
        }
    }

    //add given item to neighbours lits
    public void AddNeighbour(EBuildingType type, BaseStructure structure)
    {
        switch (type)
        {
            case EBuildingType.Wall:
                //iterate all wall positions
                for (int i = 0; i < AdjacentWallPositions.Length; i++)
                {
                    //convert position to worldspace
                    Vector3 pos = transform.TransformPoint(AdjacentWallPositions[i].Position);
                    //if position is equal to this structure position 
                    if (structure.transform.position == pos)
                    {
                        //then we add it to the adjacent wall
                        adjacentWallNeighbours.Add(structure);
                        return;
                    }
                }
                //otherwise we add it to directional wall
                adjacentDirectionaWallNeighbours.Add(structure);
                break;

            case EBuildingType.DoorFrame:
                for (int i = 0; i < AdjacentWallPositions.Length; i++)
                {
                    //convert position to worldspace
                    Vector3 pos = transform.TransformPoint(AdjacentWallPositions[i].Position);
                    //if position is equal to this structure position 
                    if (structure.transform.position == pos)
                    {
                        //then we add it to the adjacent wall
                        adjacentWallNeighbours.Add(structure);
                        return;
                    }
                }

                adjacentDirectionaWallNeighbours.Add(structure);
                break;

            case EBuildingType.Foundation:
                for (int i = 0; i < adjacentFloorPositions.Length; i++)
                {
                    //convert position to worldspace
                    Vector3 pos = transform.TransformPoint(adjacentFloorPositions[i].Position);
                    //if position is equal to this structure position 
                    if (structure.transform.position == pos)
                    {
                        //then we add it to the adjacent wall
                        adjacentFloorNeighbours.Add(structure);
                        return;
                    }
                }
                adjacentDirectionalFloorNeighbours.Add(structure);
                break;

            case EBuildingType.Floor:
                for (int i = 0; i < adjacentFloorPositions.Length; i++)
                {
                    //convert position to worldspace
                    Vector3 pos = transform.TransformPoint(adjacentFloorPositions[i].Position);
                    //if position is equal to this structure position 
                    if (structure.transform.position == pos)
                    {
                        //then we add it to the adjacent wall
                        adjacentFloorNeighbours.Add(structure);
                        return;
                    }
                }
                adjacentDirectionalFloorNeighbours.Add(structure);
                break;

            default:
                Debug.LogWarning($"Build tpye has not been specified yet. - SO_DestructionRules.cs");
                break;
        }
    }

    //Add this item as neighbour to all items its colliding with
    private void AddNeighboursForCollisions()
    {
        for (int i = 0; i < currentCollisions.Count; i++)
        {
            BaseStructure structure = currentCollisions[i].GetComponent<BaseStructure>();
            if (structure)
            {
                structure.AddNeighbour(buildType, this);
                AddNeighbour(structure.buildType, structure);
            }
        }
    }

    //remove item from neighbour list
    // called from a structure that has been destroyed
    public void RemoveNeighbour(BaseStructure structure)
    {
        if (structure.BuildType == EBuildingType.DoorFrame || structure.BuildType == EBuildingType.Wall)
        {
            DirectionalWallNeighbours.Remove(structure);
            WallNeighbours.Remove(structure);
        }
        else if (structure.BuildType == EBuildingType.Floor || structure.BuildType == EBuildingType.Foundation)
        {
            FloorNeighbours.Remove(structure);
            DirectionalFloorNeighbours.Remove(structure);
        }
    }

    //Called on the object that gets destroyed
    //and removes itself from all neighbours list
    public void OnDestruction()
    {
        foreach (BaseStructure s in FloorNeighbours)
        {
            s.RemoveNeighbour(this);
        }

        foreach (BaseStructure s in DirectionalFloorNeighbours)
        {
            s.RemoveNeighbour(this);
        }


        foreach (BaseStructure s in WallNeighbours)
        {
            s.RemoveNeighbour(this);
        }

        foreach (BaseStructure s in DirectionalWallNeighbours)
        {
            s.RemoveNeighbour(this);
        }
    }

    /// <summary>
    /// Get Adjacent tiles based on where we hit the tile.
    /// </summary>
    /// <param name="hitPoint">Point of hit in local space to the hit collider</param>
    /// <param name="position">return position. Returns vector3.zero if no position is found</param>
    /// <param name="rotation">return rotation</param>
    public virtual void GetAdjacentLocation(Vector3 hitPoint, EBuildingType typeOfBuilding, out Vector3 position, out Quaternion rotation, bool testCollision = false)
    {
        PositionHelper[] positions = GetPositionsArray(typeOfBuilding, testCollision);

        float shortestDistance = -1f;
        int shortestDistanceIndex = 0;

        //iterate all positions
        for (int i = 0; i < positions.Length; i++)
        {
            //DEBUG
            if (GLOBAL.DEBUG_BUILDING)
            {
                Vector3 debugpos = transform.TransformPoint(positions[i].Position);
                DebugExtension.DebugWireSphere(debugpos, Color.white, 1);
            }

            //calculate distance
            float dist = Vector3.Distance(hitPoint, positions[i].Position);

            //We default shortestDistance to -1 to know if we have none set
            if (shortestDistance < 0 || dist < shortestDistance)
            {
                shortestDistance = dist;
                shortestDistanceIndex = i;
            }
        }

        //double check if we have a valid index
        if (shortestDistance >= 0)
        {
            //set position and rotation to received array element
            position = positions[shortestDistanceIndex].Position;
            //add rotation on top of object rotation
            rotation = transform.rotation * Quaternion.Euler(positions[shortestDistanceIndex].Rotation);
        }
        else
        {
            // set rotation and position to zero if we get no element
            position = Vector3.zero;
            rotation = transform.rotation;
        }
    }

    private PositionHelper[] GetPositionsArray(EBuildingType typeofBuilding, bool testingCollision)
    {
        switch (typeofBuilding)
        {
            case EBuildingType.Wall:
                if (testingCollision)
                    return adjacentDirectionalWallPositions;
                else
                    return adjacentWallPositions;

            case EBuildingType.DoorFrame:
                if (testingCollision)
                    return adjacentDirectionalWallPositions;
                else
                    return adjacentWallPositions;

            case EBuildingType.Foundation:
                if (testingCollision)
                    return adjacentDirectionalFloorPositions;
                else
                    return adjacentFloorPositions;

            case EBuildingType.Floor:
                if (testingCollision)
                    return adjacentDirectionalFloorPositions;
                else
                    return adjacentFloorPositions;


            default:
                return new PositionHelper[0];
        }
    }

    #region Collision Checking
    //called on colliding structures to find out if the have neighbours in the given location
    private bool IsNeighbourPositionValid(Vector3 ComparePosition, EBuildingType type)
    {
        //iterate all possible adjacent positions for that type
        for (int i = 0; i < allPositions.Length; i++)
        {
            //convert position to localspace
            Vector3 pos = transform.InverseTransformPoint(ComparePosition);

            //return if position is ours
            if (pos == transform.position) return false;

            //check the distance between each spot
            float dist = Vector3.Distance(allPositions[i].Position, pos);

            //smaller then threshold?
            if (dist <= 0.1f)
            {
                //get a list of all neighbours based on type we are trying to place
                List<BaseStructure> neighbours = new List<BaseStructure>();
                switch (type)
                {
                    case EBuildingType.Wall:
                        neighbours = adjacentDirectionaWallNeighbours;
                        break;
                    case EBuildingType.DoorFrame:
                        neighbours = adjacentDirectionaWallNeighbours;
                        break;
                    case EBuildingType.Foundation:
                        neighbours = FloorNeighbours;
                        break;
                    case EBuildingType.Floor:
                        neighbours = FloorNeighbours;
                        break;
                    default:
                        break;
                }

                //if positions match up with an already placed tile return false
                foreach (BaseStructure s in neighbours)
                {
                    if (ComparePosition == s.transform.position)
                        return false;
                }

                //otherwise return true
                //we are able to place the tile here
                return true;
            }
        }

        return false;
    }

    private bool HasValidStructureCollision()
    {
        //iterate all the collisions to check if any of them don't return a viable adjacent position
        bool validStructure = false;
        bool InGreyArea = false;
        bool hasNonStructureCollision = false;
        for (int i = 0; i < currentCollisions.Count; i++)
        {
            //try and get the structure
            BaseStructure struc = currentCollisions[i].GetComponent<BaseStructure>();
            if (struc)
            {
                if (CompareStructure && CompareStructure == struc)
                {
                    //if its the only thing we are colliding with
                    if (currentCollisions.Count == 1)
                        return true;
                    else
                    {
                        validStructure = true;
                        continue;
                    }
                }

                if (struc.transform.position == transform.position) return false;

                //check if we have a valid adjacent tile
                validStructure = struc.IsNeighbourPositionValid(transform.position, buildType);


                //if any of the collisions fail we return false
                if (validStructure == false)
                    return false;
            }
            //if we collide with a non structure we exit
            else if (struc == null)
            {
                //For foundation it doesn't matter if we have other adjacent tiles as we want to be able to place them anywhere
                if (BuildType == EBuildingType.Foundation)
                    validStructure = IsOverlapInGreyZone(currentContactPoints[i]);
                //otherwise we only check if we have a structure to attach to
                else if (validStructure)
                    return IsOverlapInGreyZone(currentContactPoints[i]);
                else
                {
                    InGreyArea = IsOverlapInGreyZone(currentContactPoints[i]);
                    hasNonStructureCollision = true;
                }
            }
        }
        if (validStructure && hasNonStructureCollision)
            return InGreyArea;
        else
            return validStructure;
    }

    private bool IsOverlapInGreyZone(ContactPoint[] contacts)
    {
        //compare contact point with position 
        //check if it is distance is x% based on size of the object
        if (contacts.Length <= 0 || PlacementRestrictionCollider == null) return false;


        float smallestDist = -1;
        int index = 0;
        Vector3 pos = transform.TransformPoint(PlacementRestrictionCollider.center);

        //iterate all contact points 
        for (int i = 0; i < contacts.Length; i++)
        {
            //DEBUG
            if (GLOBAL.DEBUG_BUILDING)
                DebugExtension.DebugWireSphere(contacts[i].point, Color.blue, .1f);


            float dist = Vector3.Distance(pos, contacts[i].point);
            if (smallestDist <= 0)
            {
                smallestDist = dist;
                index = i;
            }
            else if (dist < smallestDist)
            {
                smallestDist = dist;
                index = i;

            }
        }

        //calculate min and max positions of the bounding box based on the collider position in world space
        Vector3 size = PlacementRestrictionCollider.size;
        Vector3 point = contacts[index].point;
        float minX = (pos.x - size.x / 2) - 0.01f;
        float maxX = (pos.x + size.x / 2) + 0.01f;
        float minY = (pos.y - size.y / 2) - 0.01f;
        float maxY = (pos.y + size.y / 2) + 0.01f;
        float minZ = (pos.z - size.z / 2) - 0.01f;
        float maxZ = (pos.z + size.z / 2) + 0.01f;

        //check if point is with in collider
        bool pointIsInside = (point.x >= minX && point.x <= maxX) &&
                  (point.y >= minY && point.y <= maxY) &&
                  (point.z >= minZ && point.z <= maxZ);

        //return if is inside or not
        return !pointIsInside;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (IsPlaced) return;
        if (collision.collider == PlacementRestrictionCollider) return;

        currentCollisions.Add(collision.collider);
        currentContactPoints.Add(collision.contacts);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (isPlaced) return;
        if (collision.collider == PlacementRestrictionCollider) return;


        int index = currentCollisions.FindIndex(d => d == collision.collider);
        currentContactPoints.RemoveAt(index);
        currentContactPoints.Insert(index, collision.contacts);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (IsPlaced) return;
        if (collision.collider == PlacementRestrictionCollider) return;

        int index = currentCollisions.IndexOf(collision.collider);
        currentCollisions.Remove(collision.collider);
        currentContactPoints.RemoveAt(index);
    }
    #endregion

    #region Debugging
    private void OnDrawGizmos()
    {
        if (Application.isPlaying == false && GLOBAL.DEBUG_BUILDING)
        {
            // white = walls
            PositionHelper[] positions = new PositionHelper[adjacentWallPositions.Length + adjacentDirectionalWallPositions.Length];
            Array.Copy(adjacentWallPositions, positions, adjacentWallPositions.Length);
            Array.Copy(adjacentDirectionalWallPositions, 0, positions, adjacentWallPositions.Length, adjacentDirectionalWallPositions.Length);


            //iterate all positions
            for (int i = 0; i < positions.Length; i++)
            {
                Vector3 debugpos = transform.TransformPoint(positions[i].Position);
                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(debugpos, .2f);
            }


            // Black = floors

            positions = new PositionHelper[adjacentFloorPositions.Length + adjacentDirectionalFloorPositions.Length];
            Array.Copy(adjacentFloorPositions, positions, adjacentFloorPositions.Length);
            Array.Copy(adjacentDirectionalFloorPositions, 0, positions, adjacentFloorPositions.Length, adjacentDirectionalFloorPositions.Length);


            //iterate all positions
            for (int i = 0; i < positions.Length; i++)
            {
                Vector3 debugpos = transform.TransformPoint(positions[i].Position);
                Gizmos.color = Color.black;
                Gizmos.DrawWireSphere(debugpos, .2f);
            }
        }
    }
    #endregion
}