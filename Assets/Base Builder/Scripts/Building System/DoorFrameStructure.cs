﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorFrameStructure : BaseStructure
{
    [SerializeField] private PositionHelper[] DoorPositions;

    private Door assignedDoorObject;
    public Door AssignedDoorObject
    {
        get { return assignedDoorObject; }
        set { assignedDoorObject = value; }
    }

    /// <summary>
    /// Get Door position based on hit in local space
    /// </summary>
    /// <param name="hitPoint">The hit point in local space to this object</param>
    /// <returns></returns>
    public PositionHelper GetDoorPosition(Vector3 hitPoint)
    {
        PositionHelper position = new PositionHelper();
        float shortestDistance = -1f;
        int shortestDistanceIndex = 0;

        //iterate all positions
        for (int i = 0; i < DoorPositions.Length; i++)
        {
            //DEBUG
            if (GLOBAL.DEBUG_BUILDING)
            {
                Vector3 debugpos = transform.TransformPoint(DoorPositions[i].Position);
                DebugExtension.DebugWireSphere(debugpos, Color.cyan, 1);
            }

            //calculate distance
            float dist = Vector3.Distance(hitPoint, DoorPositions[i].Position);

            //We default shortestDistance to -1 to know if we have none set
            if (shortestDistance < 0 || dist < shortestDistance)
            {
                shortestDistance = dist;
                shortestDistanceIndex = i;
            }
        }

        //double check if we have a valid index
        if (shortestDistance >= 0)
        {
            //set position and rotation to received array element
            position.Position = DoorPositions[shortestDistanceIndex].Position;
            //add rotation on top of object rotation
            position.Rotation = (transform.rotation * Quaternion.Euler(DoorPositions[shortestDistanceIndex].Rotation)).eulerAngles;
        }
        else
        {
            // set rotation and position to zero if we get no element
            position.Position = Vector3.zero;
            position.Rotation = transform.rotation.eulerAngles;
        }

        return position;
    }



    /// <summary>
    /// Display doorframe positions
    /// </summary>
    private void OnDrawGizmos()
    {
        if (Application.isPlaying == false && GLOBAL.DEBUG_BUILDING)
        {
            //iterate all positions
            for (int i = 0; i < DoorPositions.Length; i++)
            {
                Vector3 debugpos = transform.TransformPoint(DoorPositions[i].Position);
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(debugpos, .2f);
            }
        }
    }
}
