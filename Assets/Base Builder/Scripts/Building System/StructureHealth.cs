﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StructureHealth : MonoBehaviour
{
    [SerializeField] private EMaterialType PlacementMaterialtype = EMaterialType.twig;
    [SerializeField] private SO_StructureMaterialData[] MaterialData;
    
    private float currenthealth;
    private float maxHealth;        //retreived from materialData
    private SO_StructureMaterialData currentMaterialData;

    private BaseStructure structure;        //reference to the base structure script attached to this object
    private Door door;                      //reference to the door script attached to this object
    private MeshRenderer meshRenderer;      //reference to the meshrendererattached to this object
    private MeshFilter meshFilter;          //reference to the meshFilter attached to this object

    public UnityEvent OnDestructionEvent;   //On destruction event the base structure subscribes to

    public SO_StructureMaterialData GetCurrentMaterialData
    {
        get { return currentMaterialData; }
    }

    private void Awake()
    {
        //Get all the references
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();

        structure = GetComponent<BaseStructure>();
        if (structure)
        {
            //subscribe to the structure placement event
            structure.OnStructurePlaced += OnStructurePlaced;
        }

        door = GetComponent<Door>();
        if(door)
        {
            //subscribe to the Door placement event
            door.OnStructurePlaced += OnStructurePlaced;
        }
    }

    //structure placement event callback
    private void OnStructurePlaced()
    {
        //upgrade material on placement to twig
        UpgradeMaterial(PlacementMaterialtype);

        //if we haave material data assigned
        if (currentMaterialData)
        {
            //we save the maxhealth
            maxHealth = currentMaterialData.GetMaxHealth;
            // and set currenthealth
            currenthealth = maxHealth;
        }
    }

    /// <summary>
    /// damages the structure
    /// </summary>
    /// <param name="damageDelta">Amount to damage by></param>
    /// <param name="DamageInstigator">Who damaged the object</param>
    public void TakeDamage(float damageDelta, GameObject DamageInstigator)
    {
        if (currenthealth > 0)
        {
            currenthealth -= damageDelta;
        }
        
        if (currenthealth <= 0)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        //if we have a destroyed object 
        //we spawn it and destroy this object
        GameObject obj = currentMaterialData.GetDestroyedObject;
        if (obj)
            Instantiate(obj, transform.position, transform.rotation);
        //Call on destruction event as long as someone is subsribed to it
        OnDestructionEvent?.Invoke();
        //destroy this gameobject
        Destroy(gameObject);
    }

    /// <summary>
    /// Upgrades current material to given type
    /// </summary>
    /// <param name="type"> type of material</param>
    public void UpgradeMaterial(EMaterialType type)
    {
        if (MaterialData.Length > 0)
        {
            for (int i = 0; i < MaterialData.Length; i++)
            {
                if (MaterialData[i].GetMaterialType == type)
                {
                    currentMaterialData = MaterialData[i];
                    maxHealth = currentMaterialData.GetMaxHealth;
                    currenthealth = maxHealth;

                    UpdateMaterialAndMesh();
                }
            }
        }
    }


    ///Updates mesh and material if valid
    private void UpdateMaterialAndMesh()
    {
        Mesh newmesh = currentMaterialData.GetNewMesh;
        Material newmaterial = currentMaterialData.GetNewMaterial;
        //if we received a meshrenderer
        if (meshRenderer && newmaterial)
        {
            //we set its material to the set material in the materialdata object
            meshRenderer.sharedMaterial = newmaterial;
        }
        else
        {
            Debug.LogWarning($"Object has no meshrenderer attached!");
        }

        if(meshFilter && newmesh)
        {
            meshFilter.sharedMesh = newmesh;
        }
    }
}