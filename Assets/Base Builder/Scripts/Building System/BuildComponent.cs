﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EBuildingType
{
    Wall,
    DoorFrame,
    Foundation,
    Floor
}


public class BuildComponent : MonoBehaviour
{
    [Header("Raycast")]
    [SerializeField] private float raycastDistance = 7;
    [SerializeField] private float raycastRadius = 0.2f;
    [SerializeField] private LayerMask raycastMask;
    [SerializeField] private float maxObjectDistance = 4f;
    [SerializeField] private float floorOffset = .1f;

    [Header("Building Objects")]
    [SerializeField] private GameObject WallObject;
    [SerializeField] private GameObject DoorObject;
    [SerializeField] private GameObject FloorObject;
    [SerializeField] private GameObject FoundationObject;

    private GameObject CurrentPlaceholder;
    private BaseStructure currentStructure;

    private EBuildingType currentBuildType = EBuildingType.Foundation;
    public EBuildingType CurrentBuildType
    {
        set
        {
            currentBuildType = value;

            if (IsBuilding)
            {
                if (CurrentPlaceholder)
                {
                    Destroy(CurrentPlaceholder);
                    CurrentPlaceholder = null;
                    SpawnPlaceholder();
                }
            }
        }
    }

    private bool isBuilding;
    public bool IsBuilding
    {
        get { return isBuilding; }
        set
        {
            isBuilding = value;
            if (value == false)
            {
                if (CurrentPlaceholder != null)
                {
                    Destroy(CurrentPlaceholder);
                }
            }
        }
    }

    void Update()
    {
        if (isBuilding)
        {
            DisplayPlaceHolder();
        }
    }

    private void DisplayPlaceHolder()
    {
        if (CurrentPlaceholder == null)
        {
            SpawnPlaceholder();
        }
        else
        {
            if (currentStructure == null) return;

            Vector3 position;
            Quaternion rotation;
            GetObjectLocation(out position, out rotation);
            CurrentPlaceholder.transform.position = position;
            CurrentPlaceholder.transform.rotation = rotation;
        }
    }

    private void SpawnPlaceholder()
    {
        if (CurrentPlaceholder != null)
        {
            Destroy(CurrentPlaceholder);
            CurrentPlaceholder = null;
        }
        CurrentPlaceholder = SelectObjectToSpawn(currentBuildType);

        if (CurrentPlaceholder == null) return;

        currentStructure = CurrentPlaceholder.GetComponent<BaseStructure>();
    }

    private GameObject SelectObjectToSpawn(EBuildingType type)
    {
        GameObject spawn = default;
        switch (type)
        {
            case EBuildingType.Wall:
                if (WallObject)
                {
                    spawn = WallObject;
                    currentBuildType = EBuildingType.Wall;
                }
                break;

            case EBuildingType.DoorFrame:
                if (DoorObject)
                {
                    spawn = DoorObject;
                    currentBuildType = EBuildingType.DoorFrame;
                }
                break;

            case EBuildingType.Foundation:
                if (FoundationObject)
                {
                    spawn = FoundationObject;
                    currentBuildType = EBuildingType.Foundation;
                }
                break;

            case EBuildingType.Floor:
                if (FloorObject)
                {
                    spawn = FloorObject;
                    currentBuildType = EBuildingType.Floor;
                }
                break;
        }

        Vector3 position;
        Quaternion rotation;
        GetObjectLocation(out position, out rotation);
        if (spawn == null)
            return null;
        else
            return Instantiate(spawn, position, rotation);
    }

    private void GetObjectLocation(out Vector3 position, out Quaternion rotation)
    {
        Vector3 start = Camera.main.transform.position;
        Vector3 end = Camera.main.transform.forward;

        //   if (currentStructure == null) return;
        //we use spherecast so we can hit objects
        //even if we aren't 100% looking at them
        RaycastHit[] hits = Physics.SphereCastAll(start, raycastRadius, end, raycastDistance, raycastMask, QueryTriggerInteraction.Ignore);

        if (GLOBAL.DEBUG_BUILDING)
        {
            if (hits.Length <= 0)
                DebugExtension.DebugCapsule(start, start + (end * +raycastDistance), Color.red, raycastRadius);
        }

        //We hit nothing with our sphere cast 
        //so we set position to the end of the spherecast
        if (hits.Length <= 0)
        {
            position = start + (end * maxObjectDistance);
            rotation = transform.rotation;
            rotation *= Quaternion.Euler(0, 90, 0);
            return;
        }

        //iterate all hits
        for (int i = 0; i < hits.Length; i++)
        {
            //DEBUG
            if (GLOBAL.DEBUG_BUILDING)
            {
                //Debug spherecast
                DebugExtension.DebugCapsule(start, hits[i].point, Color.green, raycastRadius);
                //debug hit point
                DebugExtension.DebugWireSphere(hits[i].point, Color.green);
                //let us know what we hit
                Debug.Log($"HIT: {hits[i].collider.name}");
            }

            //if we hit the same object we continue as we don't count our current object as useful
            if (hits[i].collider.gameObject == CurrentPlaceholder)
            {
                continue;
            }


            //otherwise we try to cast to buildstructure 
            BaseStructure structure = hits[i].collider.transform.root.GetComponent<BaseStructure>();
            // check if object had script attached
            if (structure)
            {
                //if the structure is placed we get the desired location from the structure
                //we pass in the point we hit it at and it returns a adjacent tile slot
                if (structure.IsPlaced)
                {

                    if (IsValidStructureNeighbour(structure.BuildType) == false)
                    {

                        if (Vector3.Distance(hits[i].point, start) <= maxObjectDistance)
                        {
                            //if its the last hit we set location to the last hit
                            position = hits[i].point;
                            rotation = transform.rotation;
                            rotation *= Quaternion.Euler(0, 90, 0);
                        }
                        else
                        {
                            position = start + (end * maxObjectDistance);
                            rotation = transform.rotation;
                            rotation *= Quaternion.Euler(0, 90, 0);
                        }
                        break;
                    }

                    //give the temporary strcutre a reference to the structure we are aiming at 
                    //so we don't recheck the position as we already did that
                    if (currentStructure) currentStructure.CompareStructure = structure;

                    //we change position to local space relative to the collider we hit and not the root object
                    //as we might have a mesh and collider offset under the root object
                    Vector3 pos = hits[i].collider.transform.InverseTransformPoint(hits[i].point);

                    //get location of adjacent tile based on hit
                    structure.GetAdjacentLocation(pos, currentBuildType, out position, out rotation);

                    if (GLOBAL.DEBUG_BUILDING)
                    {
                        //debug tile position
                        DebugExtension.DebugWireSphere(position, Color.magenta, 1f);
                    }


                    //we have to check if the location is zero 
                    if (position == Vector3.zero)
                    {
                        continue;
                    }
                    //if its not zero we break the loop as we have a viable location from the tile we are aiming at
                    else if (position != Vector3.zero)
                    {
                        //convert position back to worldspace based on the hit collider transform
                        position = hits[i].collider.transform.TransformPoint(position);
                        return;
                    }
                }
            }

            //We didn't hit a structure so we set the position to the hit point
            else if (structure == null)
            {
                if (currentStructure) currentStructure.CompareStructure = null;
                if (Vector3.Distance(hits[i].point, start) <= maxObjectDistance)
                {
                    //if its the last hit we set location to the last hit
                    position = hits[i].point;
                    position.y += floorOffset;
                    rotation = transform.rotation;
                    rotation *= Quaternion.Euler(0, 90, 0);
                }
                else
                {
                    position = start + (end * maxObjectDistance);
                    rotation = transform.rotation;
                    rotation *= Quaternion.Euler(0, 90, 0);
                }
                if (i == hits.Length - 1)
                    return;

                continue;
            }
        }

        position = start + (end * maxObjectDistance);
        rotation = transform.rotation;
        rotation *= Quaternion.Euler(0, 90, 0);

    }

    private bool IsValidStructureNeighbour(EBuildingType targetType)
    {
        switch (targetType)
        {
            case EBuildingType.Wall:
                if (currentBuildType == EBuildingType.Floor)
                {
                    return true;
                }
                break;

            case EBuildingType.DoorFrame:
                if (currentBuildType == EBuildingType.Floor)
                {
                    return true;
                }
                break;

            case EBuildingType.Foundation:
                if (currentBuildType == EBuildingType.Foundation ||
                    currentBuildType == EBuildingType.Wall ||
                    currentBuildType == EBuildingType.DoorFrame)
                {
                    return true;
                }
                break;

            case EBuildingType.Floor:
                if (currentBuildType == EBuildingType.Floor ||
                    currentBuildType == EBuildingType.Wall ||
                    currentBuildType == EBuildingType.DoorFrame)
                {
                    return true;
                }
                break;
            default:
                Debug.LogError("Structure type was not defined!");
                return false;
        }

        return false;
    }


    /// <summary>
    /// Places to currently active buildingblock
    /// </summary>
    public void PlaceObject()
    {
        if (CurrentPlaceholder)
        {
            if (currentStructure && currentStructure.CanBePlaced)
            {
                currentStructure.IsPlaced = true;
                CurrentPlaceholder = null;
                currentStructure = null;
            }
        }
    }
}