﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Add more materials here
public enum EMaterialType
{
    twig,
    wood,
    stone
}

[CreateAssetMenu(fileName = "MaterialData", menuName = "Structure/MaterialData")]
public class SO_StructureMaterialData : ScriptableObject
{
    [SerializeField] private EMaterialType materialType;
    [SerializeField] private float maxHealth;
    [SerializeField] private Material newMaterial;
    [SerializeField] private Mesh newMesh;
    [SerializeField] private GameObject destroyedObject;

    public EMaterialType GetMaterialType
    {
        get { return materialType; }
    }
    public float GetMaxHealth
    {
        get { return maxHealth; }
    }
    public Material GetNewMaterial
    {
        get { return newMaterial; }
    }
    public Mesh GetNewMesh
    {
        get { return newMesh; }
    }
    public GameObject GetDestroyedObject
    {
        get { return destroyedObject; }
    }
}