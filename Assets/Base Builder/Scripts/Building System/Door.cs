﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private float openingSpeed = 1f;
    [SerializeField] private float raycastRadius;
    [SerializeField] private float raycastDistance;
    [SerializeField] private LayerMask raycastMask;

    public delegate void OnStructurePlacedEvent();
    public OnStructurePlacedEvent OnStructurePlaced;

    private bool isPlaced;
    private bool isOpened;
    private bool canOpen = true;
    private bool hasValidLocation;
    private PositionHelper lastPosition;
    private DoorFrameStructure currentDoorFrame;

    // Update is called once per frame
    void Update()
    {
        if (isPlaced == false)
            GetDoorLocation();
    }

    private void GetDoorLocation()
    {
        hasValidLocation = false;
        //Edit this to set transform either under the world or at a point at the raycast
        transform.position = new Vector3(0, -1000, 0);
        transform.rotation = Quaternion.identity;


        //fire a raycast from palyer camera 
        Vector3 start = Camera.main.transform.position;
        Vector3 end = Camera.main.transform.forward;

        RaycastHit[] hits = Physics.SphereCastAll(start, raycastRadius, end, raycastDistance, raycastMask, QueryTriggerInteraction.Ignore);

        //iterate all the hits
        for (int i = 0; i < hits.Length; i++)
        {
            //DEBUG
            if (GLOBAL.DEBUG_BUILDING)
            {
                //Debug spherecast
                DebugExtension.DebugCapsule(start, hits[i].point, Color.green, raycastRadius);
                //debug hit point
                DebugExtension.DebugWireSphere(hits[i].point, Color.green);
                //let us know what we hit
                Debug.Log($"HIT: {hits[i].collider.name}");
            }
              

            //check if we hit a doorframe
            DoorFrameStructure doorFrame = hits[i].collider.GetComponent<DoorFrameStructure>();
            if (doorFrame && doorFrame.BuildType == EBuildingType.DoorFrame)
            {
                if (doorFrame.AssignedDoorObject != null) return;   
                //convert the hit point to localspace relative to the doorframe object
                Vector3 localHit = doorFrame.transform.InverseTransformPoint(hits[i].point);
                //get door location from the frame
                PositionHelper pos = doorFrame.GetDoorPosition(localHit);
                //convert position back into world space
                Vector3 position = doorFrame.transform.TransformPoint(pos.Position);
                //set position and rotation
                transform.position = position;
                transform.rotation = Quaternion.Euler(pos.Rotation);

                // save position and rotation to use when placing
                lastPosition = new PositionHelper() { Position = position, Rotation = pos.Rotation };
                //save a reference to the frame so we can set its door component
                currentDoorFrame = doorFrame;
                hasValidLocation = true;
                return;
            }
        }
    }

    /// <summary>
    /// Places the door at the valid location
    /// </summary>
    public bool PlaceDoor()
    {
        if (hasValidLocation)
        {
            isPlaced = true;
            //invoke event if anyone is subscribed to it
            OnStructurePlaced?.Invoke();
            //set position and rotation
            transform.position = lastPosition.Position;
            //we add a little rotation to the door because the door sockets have a small rotation to show what way they will open when placed
            transform.rotation = Quaternion.Euler(lastPosition.Rotation + new Vector3(0f, -5f, 0f));

            if (currentDoorFrame) currentDoorFrame.AssignedDoorObject = this;
            gameObject.layer = LayerMask.NameToLayer("Building");

        }
        return hasValidLocation;
    }


    /// <summary>
    /// Opens or closes the door if currently available
    /// </summary>
    public void OpenCloseDoor()
    {
        if (canOpen == false) return;
        canOpen = false;
        StartCoroutine(ToggleDoorPosition());
        isOpened = !isOpened;
    }

    private IEnumerator ToggleDoorPosition()
    {
        Vector3 newRotation = transform.rotation.eulerAngles;
        Quaternion initialRotation = transform.rotation;
        if (isOpened == false)
            newRotation += new Vector3(0, 90, 0);
        else
            newRotation -= new Vector3(0, 90, 0);

        float elapsedTime = 0;

        while (elapsedTime < openingSpeed)
        {
            transform.rotation = Quaternion.Slerp(initialRotation, Quaternion.Euler(newRotation), (elapsedTime / openingSpeed));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        canOpen = true;
    }

    public void OnDestruction()
    {
        if (currentDoorFrame) currentDoorFrame.AssignedDoorObject = null;   
    }
}
