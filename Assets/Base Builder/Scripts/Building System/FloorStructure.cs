﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorStructure : BaseStructure
{

    //override can be placed to make sure we don't place adjacent to foundation
    //Just add base structure to the floor object if you do want that behavior
    public override bool CanBePlaced
    {
        get
        {
            if (CheckCollisions() == false)
                return false;
            return base.CanBePlaced;
        }
        set
        {
            if (CheckCollisions() == false)
               base.CanBePlaced = false;
            else
                base.CanBePlaced = value;
        }
    }


    /// <summary>
    /// Checks collision to see if we are colliding with a foundation
    /// If not we are good to place the object if the all other checks turn out good
    /// </summary>
    /// <returns></returns>
    private bool CheckCollisions()
    {
        for (int i = 0; i < currentCollisions.Count; i++)
        {
            BaseStructure structure = currentCollisions[i].GetComponent<BaseStructure>();
            if (structure && structure.BuildType == EBuildingType.Foundation)
                return false;
        }
        return true;
    }
}