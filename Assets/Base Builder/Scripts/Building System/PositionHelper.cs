﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PositionHelper 
{
    public Vector3 Position;
    public Vector3 Rotation;
}
