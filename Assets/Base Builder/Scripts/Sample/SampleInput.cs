﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleInput : MonoBehaviour
{
    public GameObject DoorObject;

    [SerializeField] private float raycastRadius;
    [SerializeField] private float raycastDistance;
    [SerializeField] private LayerMask raycastMask;

    private BuildComponent builder;
    private SampleDamage combat;

    private GameObject activeDoor;

    private void Start()
    {
        combat = GetComponent<SampleDamage>();
        builder = GetComponent<BuildComponent>();
    }

    void Update()
    {
        BuildInput();
        FireInput();
        Interaction();
    }

    private void BuildInput()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (builder)
            {
                builder.IsBuilding = !builder.IsBuilding;
                if (activeDoor)
                    Destroy(activeDoor);
            }
        }

        if (builder.IsBuilding)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                builder.CurrentBuildType = EBuildingType.Foundation;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                builder.CurrentBuildType = EBuildingType.Floor;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                builder.CurrentBuildType = EBuildingType.Wall;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                builder.CurrentBuildType = EBuildingType.DoorFrame;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (DoorObject && activeDoor == null)
                activeDoor = Instantiate(DoorObject);
        }

    }

    private void FireInput()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (builder && builder.IsBuilding)
            {
                builder.PlaceObject();
            }
            else if (activeDoor)
            {
                if (activeDoor.GetComponent<Door>().PlaceDoor())
                    activeDoor = null;
            }
            else if (combat)
            {
                combat.Shoot();
            }
        }

        if (Input.GetButtonDown("Fire2"))
        {
            if (combat && builder && builder.IsBuilding == false)
            {
                combat.Upgrade();
            }
        }

    }


    private void Interaction()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {

            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

            if (Physics.Raycast(ray, out RaycastHit hit, 500f, raycastMask, QueryTriggerInteraction.Ignore))
            {
                Debug.Log(hit.collider.name);
                Door door = hit.collider.GetComponent<Door>();
                if (door)
                {
                    door.OpenCloseDoor();
                }
            }

        }
    }
}
