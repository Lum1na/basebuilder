﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleDamage : MonoBehaviour
{
    [SerializeField] private LayerMask shootlayermask;
    public void Shoot()
    {
        RaycastHit hit = GetHit();
        if (hit.collider != null)
        {
            StructureHealth health = hit.collider.GetComponent<StructureHealth>();
            if (health)
            {
                health.TakeDamage(50f, gameObject);
            }
        }
    }

    public void Upgrade()
    {
        RaycastHit hit = GetHit();
        if (hit.collider != null)
        {
            {
                StructureHealth health = hit.collider.GetComponent<StructureHealth>();
                if (health)
                {
                    EMaterialType type = EMaterialType.twig;

                    switch (health.GetCurrentMaterialData.GetMaterialType)
                    {
                        case EMaterialType.twig:
                            type = EMaterialType.wood;
                            break;
                        case EMaterialType.wood:
                            type = EMaterialType.stone;
                            break;
                        case EMaterialType.stone:
                            type = EMaterialType.twig;
                            break;
                        default:
                            break;
                    }

                    health.UpgradeMaterial(type);
                }
            }
        }
    }

    private RaycastHit GetHit()
    {
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

        if (Physics.Raycast(ray, out RaycastHit hit, 500f, shootlayermask, QueryTriggerInteraction.Ignore))
        {
            Debug.Log(hit.collider.name);
            return hit;
        }
        return new RaycastHit();
    }
}
