﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DebuggingEditor : EditorWindow
{
    [MenuItem("Window/DebugHelper")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        DebuggingEditor window = (DebuggingEditor)GetWindow(typeof(DebuggingEditor));
        window.Show();
    }


    private void OnGUI()
    {
        var centeredStyle = GUI.skin.GetStyle("Label");
        centeredStyle.alignment = TextAnchor.UpperCenter;

        GUILayout.Space(10);
        GUILayout.Label("Toggle debug data",centeredStyle);
        // GUI.Label(new Rect(0, 0, 300, 20), "Toggle debug data");
        GUILayout.Space(10);
        GLOBAL.DEBUG_BUILDING = GUILayout.Toggle(GLOBAL.DEBUG_BUILDING, "Building Debugging");

    }

}
