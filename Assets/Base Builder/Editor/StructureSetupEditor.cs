﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(BaseStructure), true)]
public class StructureSetupEditor : Editor
{
    private int offsetpercent = 2;
    private bool enableArrayMaker;
    private bool WalloriginInMiddle;
    private BaseStructure structure;


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        BaseStructure structure = (BaseStructure)target;
        if (structure)
        {
            this.structure = structure;
            GUILayout.Space(20);
            GUILayout.Label("Setup", EditorStyles.boldLabel);

            enableArrayMaker = GUILayout.Toggle(enableArrayMaker, "Toggle Array creation");



            if (enableArrayMaker == false) return;

            GUILayout.Space(10);
            WalloriginInMiddle = GUILayout.Toggle(WalloriginInMiddle, "Is wall origin point in the middle of the mesh?");

            GUILayout.Space(10);
            GUILayout.Label("Edit size of the object in the GLOBAL.cs");

            if (GUILayout.Button("Fill positions arrays"))
            {
                if (EditorUtility.DisplayDialog("Create positions arrays", "Are you sure you want to create new positions arrays? This will delete any data you manually put in.", "Continue", "Cancel"))
                {
                    MakeArrays();
                }
            }
        }
    }



    private void MakeArrays()
    {
        if (structure)
        {
            switch (structure.BuildType)
            {

                case EBuildingType.Wall:
                    MakeWallTileArrays();
                    break;
                case EBuildingType.DoorFrame:
                    MakeWallTileArrays();
                    break;


                case EBuildingType.Foundation:
                    MakeFloorTileArrays();
                    break;
                case EBuildingType.Floor:
                    MakeFloorTileArrays();
                    break;

                default:
                    Debug.Log("The build type has not been defined!");
                    break;
            }
        }
    }

    private void MakeFloorTileArrays()
    {
        float full = GLOBAL.OBJECT_SIZE - ((GLOBAL.OBJECT_SIZE / 100) * 2);
        float half = full / 2;

        #region adjacent floor tiles
        PositionHelper[] FT = new PositionHelper[4];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = full;

        FT[1].Position.x = -full;

        FT[2].Position.z = full;

        FT[3].Position.z = -full;

        structure.AdjacentFloorPositions = FT;
        #endregion

        #region  adjacent directional floor tiles
        FT = new PositionHelper[4];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = full;
        FT[0].Position.z = full;

        FT[1].Position.x = full;
        FT[1].Position.z = -full;

        FT[2].Position.x = -full;
        FT[2].Position.z = full;

        FT[3].Position.x = -full;
        FT[3].Position.z = -full;

        structure.AdjacentDirectionalFloorPositions = FT;
        #endregion

        #region Adjacent Wall positions
        FT = new PositionHelper[8];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = 0;
        FT[0].Position.y = WalloriginInMiddle ?  half : 0;
        FT[0].Position.z = half;

        FT[1].Position.x = 0;
        FT[1].Position.y = WalloriginInMiddle ? half : 0;
        FT[1].Position.z = -half;

        FT[2].Position.x = half;
        FT[2].Position.y = WalloriginInMiddle ? half : 0;
        FT[2].Position.z = 0;
        FT[2].Rotation.y = 90;

        FT[3].Position.x = -half;
        FT[3].Position.y = WalloriginInMiddle ? half : 0;
        FT[3].Position.z = 0;
        FT[3].Rotation.y = 90;

        FT[4].Position.x = -half;
        FT[4].Position.y = WalloriginInMiddle ? -half : -full;
        FT[4].Position.z = 0;

        FT[5].Position.x = half;
        FT[5].Position.y = WalloriginInMiddle ? -half : -full;
        FT[5].Position.z = 0;


        FT[6].Position.x = 0;
        FT[6].Position.y = WalloriginInMiddle ? -half : -full;
        FT[6].Position.z = half;

        FT[7].Position.x = 0;
        FT[7].Position.y = WalloriginInMiddle ? -half : -full;
        FT[7].Position.z = -half;

        structure.AdjacentWallPositions = FT;
        #endregion

        #region adjacent directional wall positions
        FT = new PositionHelper[18];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = full;
        FT[0].Position.y = WalloriginInMiddle ? half : 0; 
        FT[0].Position.z = half;

        FT[1].Position.x = -full;
        FT[1].Position.y = WalloriginInMiddle ? half : 0; 
        FT[1].Position.z = -half;

        FT[2].Position.x = half;
        FT[2].Position.y = WalloriginInMiddle ? half : 0; 
        FT[2].Position.z = full;

        FT[3].Position.x = -half;
        FT[3].Position.y = WalloriginInMiddle ? half : 0; 
        FT[3].Position.z = -full;

        FT[4].Position.x = half;
        FT[4].Position.y = WalloriginInMiddle ? half : 0; 
        FT[4].Position.z = -full;

        FT[5].Position.x = full;
        FT[5].Position.y = WalloriginInMiddle ? half : 0; 
        FT[5].Position.z = -half;

        FT[6].Position.x = -full;
        FT[6].Position.y = WalloriginInMiddle ? half : 0; 
        FT[6].Position.z = half;

        FT[7].Position.x = -half;
        FT[7].Position.y = WalloriginInMiddle ? half : 0; 
        FT[7].Position.z = full;

        FT[8].Position.x = -full;
        FT[8].Position.y = WalloriginInMiddle ? -half : -full;
        FT[8].Position.z = half;

        FT[9].Position.x = -half;
        FT[9].Position.y = WalloriginInMiddle ? -half : -full;
        FT[9].Position.z = full;

        FT[10].Position.x = -half;
        FT[10].Position.y = WalloriginInMiddle ? -half : -full;
        FT[10].Position.z = full;

        FT[11].Position.x = full;
        FT[11].Position.y = WalloriginInMiddle ? -half : -full;
        FT[11].Position.z = half;

        FT[12].Position.x = -full;
        FT[12].Position.y = WalloriginInMiddle ? -half : -full;
        FT[12].Position.z = -half;

        FT[13].Position.x = half;
        FT[13].Position.y = WalloriginInMiddle ? -half : -full;
        FT[13].Position.z = full;

        FT[14].Position.x = -half;
        FT[14].Position.y = WalloriginInMiddle ? -half : -full;
        FT[14].Position.z = -full;

        FT[15].Position.x = half;
        FT[15].Position.y = WalloriginInMiddle ? -half : -full;
        FT[15].Position.z = -full;

        FT[16].Position.x = full;
        FT[16].Position.y = WalloriginInMiddle ? -half : -full;
        FT[16].Position.z = -half;

        // FT[21].Position.x = half;
        // FT[21].Position.y = half;
        // FT[21].Position.z = 0;

        structure.AdjacentDirectionalWallPositions = FT;

        #endregion

        Debug.Log("Created arrays for floor Tile!");
    }

    private void MakeWallTileArrays()
    {
        float full = GLOBAL.OBJECT_SIZE - ((GLOBAL.OBJECT_SIZE / 100) * 2);
        float half = full / 2;
        float twice = full * 2;

        //adjacent floor tiles
        PositionHelper[] FT = new PositionHelper[4];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = 0;
        FT[0].Position.y = WalloriginInMiddle ? -half : 0;
        FT[0].Position.z = half;

        FT[1].Position.x = 0;
        FT[1].Position.y = WalloriginInMiddle ? -half : 0;
        FT[1].Position.z = -half;

        FT[2].Position.x = 0;
        FT[2].Position.y = WalloriginInMiddle ? half : full;
        FT[2].Position.z = half;

        FT[3].Position.x = 0;
        FT[3].Position.y = WalloriginInMiddle ? half : full;
        FT[3].Position.z = -half;

        structure.AdjacentFloorPositions = FT;


        //adjacent directional floor tiles
        FT = new PositionHelper[8];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = -full;
        FT[0].Position.y = WalloriginInMiddle ? -half : 0;
        FT[0].Position.z = half;

        FT[1].Position.x = full;
        FT[1].Position.y = WalloriginInMiddle ? -half : 0;
        FT[1].Position.z = -half;

        FT[2].Position.x = full;
        FT[2].Position.y = WalloriginInMiddle ? -half : 0;
        FT[2].Position.z = half;

        FT[3].Position.x = -full;
        FT[3].Position.y = WalloriginInMiddle ? -half : 0;
        FT[3].Position.z = -half;

        FT[4].Position.x = -full;
        FT[4].Position.y = WalloriginInMiddle ? half : full;
        FT[4].Position.z = -half;

        FT[5].Position.x = -full;
        FT[5].Position.y = WalloriginInMiddle ? half : full;
        FT[5].Position.z = half;

        FT[6].Position.x = full;
        FT[6].Position.y = WalloriginInMiddle ? half : full;
        FT[6].Position.z = -half;

        FT[7].Position.x = full;
        FT[7].Position.y = WalloriginInMiddle ? half : full;
        FT[7].Position.z = half;

        structure.AdjacentDirectionalFloorPositions = FT;


        // adjacent wall tiles
        FT = new PositionHelper[4];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = 0;
        FT[0].Position.y = full;
        FT[0].Position.z = 0;

        FT[1].Position.x = 0;
        FT[1].Position.y = -full;
        FT[1].Position.z = 0;

        FT[2].Position.x = full;
        FT[2].Position.y = 0;
        FT[2].Position.z = 0;

        FT[3].Position.x = -full;
        FT[3].Position.y = 0;
        FT[3].Position.z = 0;

        structure.AdjacentWallPositions = FT;


        //adjacent directional wall positions
        FT = new PositionHelper[16];
        for (int i = 0; i < FT.Length; i++)
        {
            FT[i] = new PositionHelper();
        }

        FT[0].Position.x = half;
        FT[0].Position.y = 0;
        FT[0].Position.z = half;

        FT[1].Position.x = half;
        FT[1].Position.y = 0;
        FT[1].Position.z = -half;

        FT[2].Position.x = -half;
        FT[2].Position.y = 0;
        FT[2].Position.z = half;

        FT[3].Position.x = -half;
        FT[3].Position.y = 0;
        FT[3].Position.z = -half;

        FT[4].Position.x = half;
        FT[4].Position.y = full;
        FT[4].Position.z = half;

        FT[5].Position.x = half;
        FT[5].Position.y = full;
        FT[5].Position.z = -half;

        FT[6].Position.x = -half;
        FT[6].Position.y = full;
        FT[6].Position.z = half;

        FT[7].Position.x = -half;
        FT[7].Position.y = full;
        FT[7].Position.z = -half;
        FT[8].Position.x = half;
        FT[8].Position.y = -full;
        FT[8].Position.z = half;

        FT[9].Position.x = half;
        FT[9].Position.y = -full;
        FT[9].Position.z = -half;

        FT[10].Position.x = -half;
        FT[10].Position.y = -full;
        FT[10].Position.z = half;

        FT[11].Position.x = -half;
        FT[11].Position.y = -full;
        FT[11].Position.z = -half;

        FT[12].Position.x = full;
        FT[12].Position.y = full;
        FT[12].Position.z = 0;

        FT[13].Position.x = -full;
        FT[13].Position.y = full;
        FT[13].Position.z = 0;

        FT[14].Position.x = full;
        FT[14].Position.y = -full;
        FT[14].Position.z = 0;

        FT[15].Position.x = -full;
        FT[15].Position.y = -full;
        FT[15].Position.z = 0;

        structure.AdjacentDirectionalWallPositions = FT;
        Debug.Log("Created arrays for wall Tile!");
    }
}